package sk.eu.jakab.espressotestsbasic;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText txtName;
    private TextView txtMessage;
    private Button btnEnter;
    private ListView lstNames;
    private List<String>names;
    private ArrayAdapter<String> namesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //findviews
        txtMessage = (TextView) findViewById(R.id.txt_message);
        txtName = (EditText) findViewById(R.id.txt_name);
        btnEnter = (Button) findViewById(R.id.btn_enter);
        lstNames = (ListView) findViewById(R.id.lst_names);

        //adapters
        names = new ArrayList<>();
        namesAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,names);
        lstNames.setAdapter(namesAdapter);

        //listeners
        btnEnter.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.btn_enter){
            if(!txtName.getText().toString().trim().equals("")){
                txtMessage.setText("Hello "+txtName.getText());
                names.add(txtName.getText().toString());
                namesAdapter.notifyDataSetChanged();
            }else{
                Toast.makeText(this,"enter name",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
