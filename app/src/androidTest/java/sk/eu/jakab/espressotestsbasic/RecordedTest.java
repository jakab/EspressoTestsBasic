package sk.eu.jakab.espressotestsbasic;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class RecordedTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void recordedTest() {
        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txt_name), isDisplayed()));
        appCompatEditText.perform(replaceText("my"));

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txt_name), withText("my"), isDisplayed()));
        appCompatEditText2.perform(click());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txt_name), withText("my"), isDisplayed()));
        appCompatEditText3.perform(replaceText("myName"));

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btn_enter), withText("say hello"), isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction textView = onView(
                allOf(withId(R.id.txt_message), withText("Hello myName"), isDisplayed()));
        textView.check(matches(withText("Hello myName")));

    }
}
