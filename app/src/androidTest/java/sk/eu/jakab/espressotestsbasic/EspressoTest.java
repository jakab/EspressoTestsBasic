package sk.eu.jakab.espressotestsbasic;

import android.os.IBinder;
import android.support.test.espresso.Root;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.WindowManager;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.core.StringStartsWith.startsWith;

/**
 * Created by jakab on 4. 8. 2016.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4.class)
public class EspressoTest {
    private String myName = "myName";

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);


    //components are visible
    @Test
    public void test01(){
        onView(withId(R.id.btn_enter)).check(matches(isDisplayed()));
        onView(withId(R.id.txt_name)).check(matches(isDisplayed()));
        onView(withId(R.id.txt_message)).check(matches(isDisplayed()));
        //== onView(withText("Hello %$@")).check(matches(isDisplayed()));
    }

    //"enter name" toast
    @Test
    public void test02(){
        onView(withId(R.id.btn_enter)).perform(click());
        onView(withText("enter name")).inRoot(new ToastMatcher())
                .check(matches(isDisplayed()));
    }

    //enter name
    @Test
    public void test03(){
        onView(withId(R.id.txt_name))
                .perform(click())
                .perform(clearText())
                .perform(typeText(myName));

        onView(withId(R.id.txt_name)).check(matches(withText(myName)));
    }



    //name appear in message
    @Test
    public void test04(){
        test03();
        onView(withId(R.id.btn_enter)).perform(click());
        onView(withId(R.id.txt_message)).check(matches(withText("Hello "+myName)));
    }

    //name appear in history
    @Test
    public void test05() throws InterruptedException {
        test04();
        closeSoftKeyboard();

        //without Thread.sleep(1000) test not pass
        //reason: listView is not displayed to user - only visible components can be asserted
        //closing soft keyboard takes some time. right solution is to disable animations on device
        Thread.sleep(1000);

        onData(hasToString(startsWith(myName)))
                .inAdapterView(withId(R.id.lst_names))
                .check(matches(isDisplayed()));
    }



    public class ToastMatcher extends TypeSafeMatcher<Root> {

        @Override
        public void describeTo(Description description) {
            description.appendText("is toast");
        }

        @Override
        public boolean matchesSafely(Root root) {
            int type = root.getWindowLayoutParams().get().type;
            if ((type == WindowManager.LayoutParams.TYPE_TOAST)) {
                IBinder windowToken = root.getDecorView().getWindowToken();
                IBinder appToken = root.getDecorView().getApplicationWindowToken();
                if (windowToken == appToken) {
                    //means this window is not contained by any other windows.
                    return true;
                }
            }
            return false;
        }
    }
}
